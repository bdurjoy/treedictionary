import java.util.TreeMap;


class Dictionary {
	TreeMap<String, Integer> treeMap;
	
	public Dictionary() {
		treeMap = new TreeMap<String, Integer>();
	}
	
	public String formatWord(String word) {
		return word.toLowerCase().replaceAll("[^A-Za-z0-9]", "");
	}
	
	public void resetTree() {
		treeMap.clear();
	}

	public void insertInDictionary(String wordInsert) {
		// TODO Auto-generated method stub
		String word = formatWord(wordInsert);
		int size;
		if(treeMap.containsKey(word)) {
			size = treeMap.get(word);
		}else {
			size = 0;
		}
		treeMap.put(word, (size + 1));
		
	}

	public void searchWord(String wordSearch) {
		// TODO Auto-generated method stub
		if(treeMap.containsKey(wordSearch)) {
			System.out.println(wordSearch+" occurs "+treeMap.get(wordSearch)+" times");
		}else {
			System.err.println("Not available in the list!");
		}
	}

	public void displayNodeNumb() {
		// TODO Auto-generated method stub
//		int total = 0;
//		for(int nodeSum: treeMap.values()) {
//			total++;
//		}
		System.out.println("There are: "+treeMap.size()+" nodes");
	}
}