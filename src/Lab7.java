import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Lab7 {
	private Dictionary dics = new Dictionary();
	public boolean input(Scanner keyboard, String userInput) {
		
		
		switch(userInput) {
		case "1":
			dics.resetTree();
			break;
		case "2":
			System.out.println("Enter Word to insert in the dictionary:");
			String wordInsert = keyboard.next();
			dics.insertInDictionary(wordInsert);
			break;
		case "3":
			System.out.println("Enter the file name: ");
			String fileName = keyboard.next();
			try {
				File fileRead = new File(fileName); //fileread that takes input of file name
				Scanner reader = new Scanner(fileRead);//String fileData;
				//word by word file reading and insert it in the tree
				String data;
				while(reader.hasNext()) {
					data = reader.next();
					dics.insertInDictionary(data);
				}
			}catch(FileNotFoundException fN) {
				System.err.println("Sorry! File Not available.");
			}
			break;
		case "4":
			System.out.println("Enter word to search for: ");
			String wordSearch = keyboard.next();
			dics.searchWord(wordSearch);
			break;
		case "5":
			System.out.println("Display number of nodes: ");
			dics.displayNodeNumb();
			break;
		case "6":
			return true;
			default:
				System.out.println("Invalid Input! ");
		}
		return false;
	}
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		boolean finish = false;
		Lab7 lab = new Lab7();
		
		while(finish == false) {
			System.out.println("Enter"
				+ "\t1. To reset the tree\n"
				+ "\t2. To read a string of text from keyboard\n"
				+ "\t3. To read text from a file and add to dictionary\n"
				+ "\t4. To Search word and show the iteration times\n"
				+ "\t5. To display number of nodes in the dictionary\n"
				+ "\t6. To exit: ");
			
			String userInput = keyboard.next();
			finish = lab.input(keyboard, userInput);
		}
	}
}
